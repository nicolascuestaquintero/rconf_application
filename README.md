# README #

In this repository you will find a sample .xml file and an R file with three funcitons that
will allow you to get data from the xml easily into a tibble, provided that you start in a node 
where the tables are defined, with variables and observations defined in the following nodes.
It works by reading nodes recursively calling and playing with functions environments.

### What is this repository for? ###

This is a sample of my work with R to apply for the Diversity and International scholarships. 

### Why are these functions useful? ###

Although sample data may not reflect the importance of these functions, I though it could be a cool 
script because in the Real Estate industry multiple observations are saved in different nodes with 
different lengths, e.g. for a CMBS you can have multiple mortgages, each mortgage with its own 
description and amortization cash flow, and may or may not include options or covenants. Therefore, 
at the CMBS node you will find different lengths because each debt may or may not have these features, 
and each feature can vary as well. In fact, there are a lot of "UserDefinedFields".

Data is a modified version of: https://www.tutorialspoint.com/r/r_xml_files.htm
